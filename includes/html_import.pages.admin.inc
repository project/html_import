<?php

/**
 * @file
 * HTML Import admin page.
 */

/**
 * Form API callback for administration settings form.
 */
function html_import_admin_form($form, $form_state = array()) {

  $form['html_import_import'] = array(
    '#type' => 'fieldset',
    '#title' => t('HTML Import - consolidate imported pages'),
    '#description' => t('Consolidate imported book pages and save them to a full-text field for index'),
  );
  $form['html_import_import']['html_import_advpubs_process'] = array(
    '#type' => 'submit',
    '#value' => t('Consolidate'),
    '#suffix' => '<span class="description">' . t('This could take time and might put an increased load on your server.') . '</span>',
  );
  $form['#submit'][] = '_html_import_import_submit';

  return $form;
}

/**
 * Form submission handler for html_import_admin_form().
 *
 * @param $form
 * @param $form_state
 */
function _html_import_import_submit($form, &$form_state) {
  _html_import_import_process();
}

/**
 * Helper batch function to consolidate imported pages.
 */
function _html_import_import_process() {
  $books = db_query('SELECT DISTINCT bid FROM {book}')->fetchAll();
  // Using Batch API for bulk processing
  if (count($books) > 0) {
    $batch = array(
      'title' => t('HTML Import - Consolidate imported pages'),
      'operations' => array(),
      'finished' => '_html_import_import_batch_finished',
      'init_message' => t('Initializing...'),
      'progress_message' => t('Processing @current out of @total books.'),
      'error_message' => t('Processing encountered an error.'),
      'file' => drupal_get_path('module', 'html_import') . '/includes/html_import.batch.inc',
    );
    foreach ($books as $book) {
      $batch['operations'][] = array(
        '_html_import_import_batch_process_entity',
        array(
          $book,
          'consolidate',
        ),
      );
    }
    batch_set($batch);
    batch_process();
  }
}
