<?php
/**
 * @file
 * html_import_example.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function html_import_example_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'html_import';
  $feeds_importer->config = array(
    'name' => 'HTML Import',
    'description' => 'Import HTML into a book structure',
    'fetcher' => array(
      'plugin_key' => 'HtmlImportFetcher',
      'config' => array(
        'allowed_extensions' => 'html htm',
        'direct' => 0,
        'directory' => 'public://feeds/html_import',
        'allowed_schemes' => array(
          'public' => 'public',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'HtmlImportParser',
      'config' => array(),
    ),
    'processor' => array(
      'plugin_key' => 'HtmlImportProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => 0,
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'title',
            'target' => 'title',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'content',
            'target' => 'body',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'footnotes',
            'target' => 'field_footnotes',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'book_id',
            'target' => 'field_publication_parent:etid',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '2',
        'update_non_existent' => 'skip',
        'input_format' => 'full_html',
        'skip_hash_check' => 0,
        'bundle' => 'publication_section',
      ),
    ),
    'content_type' => 'publication',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['html_import'] = $feeds_importer;

  return $export;
}
